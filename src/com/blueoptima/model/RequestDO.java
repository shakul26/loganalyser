package com.blueoptima.model;

import java.util.Map;
import java.util.stream.Collectors;

public class RequestDO {

	private String date;
	private String time;
	private String logLevel;
	private Map<String, String> attributeMap;

	public RequestDO(String date, String time, String logLevel, Map<String, String> attributeMap) {
		this.date = date;
		this.time = time;
		this.logLevel = logLevel;
		this.attributeMap = attributeMap;
	}

	@Override
	public String toString() {
		return "RequestDO [Date=" + date + " : time=" + time + " : logLevel=" + logLevel + " : attributeMap="
				+ (getMapAsString(attributeMap)) + "]";
	}

	private String getMapAsString(Map<String, String> map) {
		String mapAsString = map.keySet().stream()
	      .map(key -> key + "=" + map.get(key))
	      .collect(Collectors.joining(", ", "{", "}"));
		return mapAsString;
	}
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	public Map<String, String> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeMap == null) ? 0 : attributeMap.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((logLevel == null) ? 0 : logLevel.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestDO other = (RequestDO) obj;
		if (attributeMap == null) {
			if (other.attributeMap != null)
				return false;
		} else if (!attributeMap.equals(other.attributeMap))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (logLevel == null) {
			if (other.logLevel != null)
				return false;
		} else if (!logLevel.equals(other.logLevel))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
	
	

}
