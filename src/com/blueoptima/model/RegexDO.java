package com.blueoptima.model;

import java.util.regex.Pattern;

public class RegexDO {

	private Pattern pattern;
	private int matchCount;

	public RegexDO(String pattern) {

		this.pattern = Pattern.compile(pattern);
	}

	public RegexDO(Pattern pattern) {
		this.pattern = pattern;

	}

	public Pattern getPattern() {
		return pattern;
	}

	public int getMatchCount() {
		return matchCount;
	}

	public void incrementMatchCount() {
		this.matchCount++;
	}

	public boolean isRegexNotMatchedYet() {

		return this.matchCount == 0;
	}

	public boolean matches(String word) {

		boolean retVal = this.pattern.matcher(word).matches();
		if (retVal)
			incrementMatchCount();

		return retVal;
	}

	@Override
	public String toString() {
		return "RegexDO [pattern=" + pattern + ", matchCount=" + matchCount + "]";
	}
	
	
}
