package com.blueoptima;

import static com.blueoptima.constant.Constant.LOG_EVENT_BUFFER_SIZE;
import static com.blueoptima.constant.Constant.THREAD_POOL_SIZE;

public class LogApplication {

	public static void main(String[] args) {

		LogAnalyser logAnalyser = new LogAnalyser(LOG_EVENT_BUFFER_SIZE, THREAD_POOL_SIZE);
		logAnalyser.initLogAnalysis();
	}
}
