package com.blueoptima;

import static com.blueoptima.constant.Constant.API_REGEX;
import static com.blueoptima.constant.Constant.ENTERPRISE_ID_REGEX;
import static com.blueoptima.constant.Constant.ENTERPRISE_NAME_REGEX;
import static com.blueoptima.constant.Constant.IP_ADDRESS_REGEX;
import static com.blueoptima.constant.Constant.LOG_FILE_LOCATION;
import static com.blueoptima.constant.Constant.REQUEST_TYPE_REGEX;
import static com.blueoptima.constant.Constant.STATUS_CODE_REGEX;
import static com.blueoptima.constant.Constant.USER_AGENT_REGEX;
import static com.blueoptima.constant.Constant.USER_REGEX;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.blueoptima.cleaner.Cleaner;
import com.blueoptima.cleaner.PunctuationMarkCleaner;
import com.blueoptima.constant.Constant;
import com.blueoptima.extractor.Extractor;
import com.blueoptima.extractor.TextFileExtractor;
import com.blueoptima.model.RegexDO;
import com.blueoptima.model.RequestDO;
import com.blueoptima.parser.Parser;
import com.blueoptima.parser.TextFileParser;;

public class LogAnalyser {

	private final BlockingQueue<String> buffer;
	private ExecutorService executor = null;
	private List<RequestDO> parsedList = new CopyOnWriteArrayList<>();
	// private Map<String, RegexDO> attributeRegexMap;

	public LogAnalyser(int bufferSize, int threadPoolSize) {

		this.buffer = new ArrayBlockingQueue<String>(bufferSize);
		executor = Executors.newFixedThreadPool(threadPoolSize);
		// this.attributeRegexMap = getAttributeRegexMap();

	}

	public void initLogAnalysis() {

		try {

			initExtractor();
			initParser();
			print();

		} catch (Exception e) {
			System.out.println("Exception Occurred while Log Analysis :: " + e.getMessage());
		} finally {
			System.out.println("Shuting down executor...");
			if (executor != null) {
				executor.shutdown();
				try {
					if (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
						executor.shutdownNow();

					}
				} catch (InterruptedException e) {
					executor.shutdownNow();
				}
			}

		}
	}

	/**
	 * Producer Method which reads the file and puts the events into queue
	 * 
	 * 1 producer
	 * 
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public void initExtractor() throws InterruptedException, ExecutionException {

		Extractor extractor = new Extractor(new TextFileExtractor(LOG_FILE_LOCATION, buffer));
		executor.execute(extractor);

	}

	/**
	 * Consumer method, multiple consumer consumes the request data present in queue
	 * and process them.
	 * 
	 * @throws InterruptedException
	 */
	public void initParser() throws InterruptedException {
		Cleaner punctuationCleaner = new PunctuationMarkCleaner();
		while (true) {
			String requestData = buffer.take();
			requestData = punctuationCleaner.clean(requestData);
			if (requestData.equalsIgnoreCase(Constant.POISON))
				break;
			Parser parser = new Parser(new TextFileParser(getAttributeRegexMap()), requestData, parsedList);
			executor.execute(parser);
		}
	}

	private void print() throws InterruptedException, ExecutionException {
		Thread.sleep(2000);
		for (RequestDO f : parsedList) {
			System.out.println(f);
		}
	}

	/**
	 * Method return a Map where key is the attribute name and Value is the RegexDO
	 * pattern for that key
	 * 
	 * @return
	 */
	private Map<String, RegexDO> getAttributeRegexMap() {
		Map<String, RegexDO> attributeregexMap = new HashMap<>();

		attributeregexMap.put("ip_address", new RegexDO(IP_ADDRESS_REGEX));
		attributeregexMap.put("User_Agent", new RegexDO(USER_AGENT_REGEX));
		attributeregexMap.put("Status_Code", new RegexDO(STATUS_CODE_REGEX));
		attributeregexMap.put("Request_Type", new RegexDO(REQUEST_TYPE_REGEX));
		attributeregexMap.put("API", new RegexDO(API_REGEX));
		attributeregexMap.put("User", new RegexDO(USER_REGEX));
		attributeregexMap.put("Enterprise_Id", new RegexDO(ENTERPRISE_ID_REGEX));
		attributeregexMap.put("Enterprise_Name", new RegexDO(ENTERPRISE_NAME_REGEX));
		return attributeregexMap;
	}

}
