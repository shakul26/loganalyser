package com.blueoptima.extractor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Pattern;

import com.blueoptima.constant.Constant;

import static com.blueoptima.constant.Constant.POISON;

public final class TextFileExtractor implements Extractable {

	private final BlockingQueue<String> QUEUE;
	private final String FILE_LOCATION;
	private transient BufferedReader bufferedReader;

	public TextFileExtractor(String fileLocation, BlockingQueue<String> queue) {
		this.FILE_LOCATION = fileLocation;
		this.QUEUE = queue;
	}

	@Override
	public void extract() {
		String line = null;

		try {

			StringBuilder requestData = new StringBuilder();
			boolean isRequestDataFormed = false;
			int dateTimelineCount = 0;
			bufferedReader = new BufferedReader(new FileReader(FILE_LOCATION));
			
			while ((line = bufferedReader.readLine()) != null) {
				// if a line has date time as prefix increment the line count
				if (Pattern.compile(Constant.DATE_TIME_PREFIX_REGEX).matcher(line).matches())
					++dateTimelineCount;
				
				// keep appending line till another line having date time is found
				if (dateTimelineCount < 2)
					requestData.append(line);
				else
					isRequestDataFormed = true;

				if (isRequestDataFormed) {
					QUEUE.put(requestData.toString().trim());
					requestData = new StringBuilder(line);
					isRequestDataFormed = false;
					dateTimelineCount = 1;
				}
			}
			// for last request data
			QUEUE.put(requestData.toString().trim());
			
			// putting special symbol in queue to notify consumer about producer has
			// finished producing
			QUEUE.put(POISON);

		} catch (IOException e) {
			System.out.println("Problem reading the log file!");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
