package com.blueoptima.extractor;

public interface Extractable {

	public void extract();
}
