package com.blueoptima.extractor;

public class Extractor implements Runnable{

	private Extractable extractStrategy;
	
	public Extractor(Extractable extractStrategy) {
		this.extractStrategy = extractStrategy;
		
	}


	@Override
	public void run() {
		
		extractStrategy.extract();
	}
	
	
	


}
