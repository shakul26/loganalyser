package com.blueoptima.constant;

public class Constant {

	public static final String LOG_FILE_LOCATION = "C:\\Users\\shakul.dubey\\Desktop\\log.txt";
	public static final String POISON = "*NA*";
	public static final int LOG_EVENT_BUFFER_SIZE = 10;
	public static final int THREAD_POOL_SIZE = 3;
	
	/**
	 * Date  yyyy-mm-dd
	 * Time  hh:mm:ss hours [ from 00 to 23 ], minutes [ from 00 to 59 ], seconds [ from 00 to 59 ]
	 */
	public static final String DATE_TIME_PREFIX_REGEX = "^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])\\s(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d.*)";
	public static final String DATE_REGEX = "[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])";
	public static final String TIME_REGEX = "(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)";
	public static final String LOG_LEVEL_REGEX = "(\\b(?:error|ERROR|info|INFO|warn|WARN|debug|DEBUG|trace|TRACE|fatal|FATAL|SEVERE|severe|warning|WARNING|fine|FINE|finer|FINER|finest|FINEST|config|CONFIG)\\b)";
	public static final String IP_ADDRESS_REGEX = "^IP-Address=\\b(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b";
	public static final String USER_AGENT_REGEX = "^User-Agent=.*";
	public static final String STATUS_CODE_REGEX = "^Status-Code=.*";
	public static final String REQUEST_TYPE_REGEX = "^Request-Type=.*";
	public static final String API_REGEX = "^API=.*";
	public static final String USER_REGEX = "^User-Login=.*";
	public static final String ENTERPRISE_ID_REGEX = "^EnterpriseId=.*";
	public static final String ENTERPRISE_NAME_REGEX = "^EnterpriseName=.*";
	
}
