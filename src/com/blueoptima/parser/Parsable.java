package com.blueoptima.parser;

import com.blueoptima.model.RequestDO;

public interface Parsable {

	public RequestDO parse(String requestData);
}
