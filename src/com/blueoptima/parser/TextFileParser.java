package com.blueoptima.parser;

import static com.blueoptima.constant.Constant.DATE_REGEX;
import static com.blueoptima.constant.Constant.LOG_LEVEL_REGEX;
import static com.blueoptima.constant.Constant.TIME_REGEX;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import com.blueoptima.model.RegexDO;
import com.blueoptima.model.RequestDO;

public class TextFileParser implements Parsable {

	private Map<String, RegexDO> attributeRegexMap;
	private Map<String, String> attributeMap;
	public TextFileParser(Map<String, RegexDO> attributeRegexMap) {
		this.attributeRegexMap = attributeRegexMap;
		this.attributeMap = new HashMap<>();
	}

	@Override
	public RequestDO parse(String requestData) {

		StringTokenizer tokens = new StringTokenizer(requestData);
		RegexDO dateRegexDO = new RegexDO(Pattern.compile(DATE_REGEX));
		RegexDO timeRegexDO = new RegexDO(Pattern.compile(TIME_REGEX));
		RegexDO logLevelRegexDO = new RegexDO(Pattern.compile(LOG_LEVEL_REGEX));

		String date = null;
		String time = null;
		String logLevel = null;


		while (tokens.hasMoreTokens()) {
			String word = tokens.nextToken();
			
			if (dateRegexDO.isRegexNotMatchedYet() && dateRegexDO.matches(word)) {
				date = word;
			} else if (timeRegexDO.isRegexNotMatchedYet() && timeRegexDO.matches(word)) {
				time = word;
			} else if (logLevelRegexDO.isRegexNotMatchedYet() && logLevelRegexDO.matches(word)) {
				logLevel = word;
			} else {

				for (String attribute : attributeRegexMap.keySet()) {

					RegexDO regexDO = attributeRegexMap.get(attribute);
					
					if (regexDO.isRegexNotMatchedYet() && regexDO.matches(word)) 
						attributeMap.put(attribute, word.split("=")[1]);
						
					
				}
			}

		}

		RequestDO requestDO = new RequestDO(date, time, logLevel, attributeMap);

		return requestDO;

	}
}
