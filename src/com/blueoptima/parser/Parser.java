package com.blueoptima.parser;

import java.util.List;

import com.blueoptima.model.RequestDO;

public class Parser implements Runnable {

	private Parsable parsingStrategy;
	private List<RequestDO> parsedList;
	private String requestData = null;

	public Parser(Parsable parsingStrategy, String requestData, List<RequestDO> parsedList) {
		this.parsingStrategy = parsingStrategy;
		this.requestData = requestData;
		this.parsedList = parsedList;

	}

	@Override
	public void run() {
		RequestDO parsedDO = parsingStrategy.parse(requestData);
		parsedList.add(parsedDO);
	}

	public List<RequestDO> getParsedList() {
		return parsedList;
	}

}
