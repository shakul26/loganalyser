package com.blueoptima.cleaner;

public interface Cleaner {

	public String clean(String line);
}
