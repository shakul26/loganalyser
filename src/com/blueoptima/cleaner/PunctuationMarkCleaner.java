package com.blueoptima.cleaner;

/**
 * This class 
 * @author Shakul.Dubey
 *
 */
public class PunctuationMarkCleaner implements Cleaner {

	@Override
	public String clean(String line) {

		line = line.replaceAll("[(),!#;]", " ");
		return line;
	}

}
