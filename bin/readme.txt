# Log Analyser
Log Analyser takes a log file as input and provides the requeired information from the log file.

Log Analyser is divided into three modules.

1. Extractor : Extracts the data from log file and keeps into a queue.
2. Parser : Reads the data from the queue and converts it into a structured form.


## Assumptions

### Extractor

1. It will consider all data between two time stamp as single request.
2. Time stamp is in the format yyyy-mm-dd hh:mm:ss

### Parser

1. Assumes each each request will begin with time stamp.
2. Time stamp is in the format yyyy-mm-dd hh:mm:ss
3. for a given attribute if multiple values are present then all values are returned with concatenation with special character
4. It will parse only one value of matching regex if a given line contain multiple matches


888888888888888888888
## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
